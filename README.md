# TruckSafety

## Getting started

Make sure to have the following:

For Text Editor, use [Visual Studio Code](https://code.visualstudio.com/)

For Unity, use version: 2021.3.15
If you are on Linux, You must first install [Unity Hub](https://docs.unity3d.com/hub/manual/InstallHub.html#install-hub-linux)
Then, when you open Unity Hub for the first time, you will asked to login to your account, so if you haven't do so create an account and login
Once logged in, exit Unity Hub
Then, open a terminal window and type the following:
```
unityhub unityhub://2021.3.15f1/e8e88683f834
```

The previous command allows you to install Unity Version 2021.3.15

If you are on Windows, you can download and install [Unity Hub](https://unity.com/download)
You can then install the 2021.3.15 version of Unity by following this [guide](https://support.unity.com/hc/en-us/articles/4402520309908-How-do-I-add-a-version-of-Unity-that-does-not-appear-in-the-Hub-installs-window-)


## Downloading and setting up Project

To download the Unity project, use [git](https://git-scm.com/)

```
git clone https://gitlab.com/tahfimullatif/trucksafety.git

```

Once git finishes downloading the project using the previous command, open Unity Hub and click on the Projects tab.
Then, click ADD and find the folder containing the project, then click ok.
Once the project is loaded, check under the Unity Version column for the project and click on the dropdown menu to select version 2021.3.15.
Once that's done, click on the project to load it.
If you encounter the Unity Package Manager Error, hit Continue. Ignore the following error:
```
An error occurred while resolving packages:
  Project has invalid dependencies:
    com.unity.render-pipelines.universal: Package [com.unity.render-pipelines.universal@12.1.9] cannot be found
```    

## Running project on Android device

Now that the project is loaded, click on File -> Build Settings and under Platform select Android and then click Switch Platform. Then connect your Android device to your computer. If your Android device prompts you with messages along the lines of Allow USB Connection, press Allow. Then on Unity, click File -> Build and Run. Again if you get a message on your Android device to Allow connection, press Allow. Once the Build and Run process finishes, you should see the app launch on your Android device with a Unity Logo and you have successfully loaded the game on your Android Device. Just for future reference, you can find the game under the name ExampleBlender on your Android Device.