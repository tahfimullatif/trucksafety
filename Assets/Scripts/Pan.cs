using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pan : MonoBehaviour{
    Vector3 touchStart;
    float zoomOutMin = 1.0f;
    float zoomOutMax = 170.0f;
    public float perspectiveZoomSpeed = 0.5f;
    void Start()
    {
        
      
    }

    void Update()
    {
        if(Camera.main != null)
        {            
            if(Input.GetMouseButtonDown(0))
            {
                touchStart = Camera.main.ScreenToWorldPoint(Input.mousePosition + new Vector3(0, 0, 20f));
                Debug.Log(touchStart);
            }

            if(Input.touchCount == 2)
            {
                Touch touchZero = Input.GetTouch(0);
                Touch touchOne = Input.GetTouch(1);

                Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
                Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

                float prevMagnitude = (touchZeroPrevPos-touchOnePrevPos).magnitude;
                float currMagnitude = (touchZero.position - touchOne.position).magnitude;

                float difference = prevMagnitude - currMagnitude;

                // zoom(difference*0.01f);
                zoom(difference);

            }
            else if (Input.touchCount == 3)
            {
                Touch touch = Input.GetTouch(0);

                Debug.Log("Input touch count > 0");

                if(touch.phase == TouchPhase.Moved)
                {
                    Debug.Log("Touch Phase Moved");
                    Quaternion rotate_z = Quaternion.Euler(0f, 0f, -touch.deltaPosition.x * 0.1f);
                    Camera.main.transform.rotation = Camera.main.transform.rotation * rotate_z;
                }
            }
            else if(Input.GetMouseButton(0))
            {
                Vector3 direction = touchStart - Camera.main.ScreenToWorldPoint(Input.mousePosition + new Vector3(0, 0, 20f));
            
                Camera.main.transform.position += direction;
            }
        }    
    }


    // void zoom(float increment)
    // {
       
        // Camera.main.fieldOfView = Mathf.Clamp(Camera.main.fieldOfView-increment, zoomOutMin, zoomOutMax);
    // }

    void zoom(float deltaMagnitudeDiff)
    {
        Camera.main.fieldOfView += deltaMagnitudeDiff * perspectiveZoomSpeed;
        Camera.main.fieldOfView = Mathf.Clamp(Camera.main.fieldOfView, zoomOutMin, zoomOutMax);
    }
}