using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class SphereClick : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        GameObject g = GameObject.FindWithTag("sphere");
        // Debug.Log(g.transform.position);
        g.transform.position = new Vector3(0, g.transform.position.y, 1000);
        GameObject g_2 = GameObject.FindWithTag("sphere_2");
        // Debug.Log(g.transform.position);
        g_2.transform.position = new Vector3(0, g.transform.position.y, -1000);
        
        GameObject g_3 = GameObject.FindWithTag("sphere_3");
        // Debug.Log(g.transform.position);
        g_3.transform.position = new Vector3(1000, g.transform.position.y, 0);
        
        GameObject g_4 = GameObject.FindWithTag("sphere_4");
        // Debug.Log(g.transform.position);
        g_4.transform.position = new Vector3(-1000, g.transform.position.y, 0);
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnMouseDown()
    {
        Debug.Log("Sphere Clicked");
        // g.SetActive(true);
    }
}
