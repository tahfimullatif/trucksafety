
public class StreetDialogInfo
{
    private string sensor;
    private string streetName;

    public StreetDialogInfo()
    {

    }

    public StreetDialogInfo(string sensor, string streetName)
    {
        this.sensor=sensor;
        this.streetName = streetName;
    }

    public string Sensor {
        get{return sensor;}
        set{sensor = value;}
    }

    public string StreetName{
        get{return streetName;}
        set{streetName = value;}
    }
}