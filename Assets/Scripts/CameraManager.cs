using UnityEngine;
using TMPro;

public class CameraManager : MonoBehaviour{
public Camera firstPersonCam;
public Camera thirdPersonCam;

private TextMeshProUGUI camSwitchBtnText;

private bool whichCam;

void Start()
{
  GameObject camSwitchTextBox = GameObject.FindWithTag("cam_view_button_text");
  camSwitchBtnText = camSwitchTextBox.GetComponent<TextMeshProUGUI>();
}

public void switchCam()
{
     if(whichCam)
     {
       whichCam = false;
       showFollowCam();
       camSwitchBtnText.text="\\/";
     }
     else
     {
       whichCam = true;
       showMapManuverCam();
       camSwitchBtnText.text = "/\\";
     }
}

private void showFollowCam()
{
      firstPersonCam.enabled = true;
      thirdPersonCam.enabled = false;
}

private void showMapManuverCam()
{
      firstPersonCam.enabled = false;
      thirdPersonCam.enabled = true;
}
}
