public class CautionDialogInfo {
    private string sensor;
    private string objectName;

    public CautionDialogInfo()
    {

    }

    public CautionDialogInfo(string sensor, string objectName)
    {
        this.sensor = sensor;
        this.objectName = objectName;
    }

    public string Sensor {
        get{return sensor;}
        set{sensor = value;}
    }

    public string ObjectName{
        get{return objectName;}
        set{objectName = value;}
    }
}