using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class DialogManager : MonoBehaviour
{
    CautionDialogInfo cautionDialogInfo;
    public GameObject dialogBox;
    public TextMeshProUGUI dialogBoxText;
    public GameObject streetInfoDialogBox;
    public TextMeshProUGUI streetInfoDialogBoxText;
    public GameObject truckOffRouteReminderBox;
    static private DialogManager __Current;
    // Start is called before the first frame update
    void Start()
    {
     cautionDialogInfo = new CautionDialogInfo();   
    }

    public void StartDialog(CautionDialog cautionDialog)
    {
        string prompt_a = string.Concat("The ",cautionDialog.cautionDialogInfo.Sensor);
        string prompt_b = string.Concat(prompt_a, " is blocked by ");
        string prompt_c = string.Concat(cautionDialog.cautionDialogInfo.ObjectName, ". Would you like to move the object?");
        prompt_c = string.Concat(prompt_b, prompt_c);
        dialogBoxText.text = prompt_c;
        dialogBox.SetActive(true);
        cautionDialogInfo = cautionDialog.cautionDialogInfo;
        
    }

    public void StartDialog(StreetInfoDialog streetInfoDialog)
    {
        string streetInfo = string.Concat("You are on street ", streetInfoDialog.streetDialogInfo.StreetName.Substring(2));
        streetInfoDialogBoxText.text = streetInfo;
        streetInfoDialogBox.SetActive(true);
    }

    public void startTruckOffRouteReminderDialog()
    {
        truckOffRouteReminderBox.SetActive(true);
    }

    public void yes_response()
    {
        GameObject obj = GameObject.Find(cautionDialogInfo.ObjectName);
        if(obj.transform.position.x > obj.transform.position.z)
        {
            obj.transform.position += new Vector3(50, 0, 0);
        }
        else if (obj.transform.position.z > obj.transform.position.x)
        {
            obj.transform.position += new Vector3(0, 0, 50);
        }
        dialogBox.SetActive(false);
        TruckSensorsManager.Current.unreportAllSensorsDetection();
        TruckSensorsManager.Current.cautionDialogDismissed();
    }

    public void no_response()
    {
        dialogBox.SetActive(false);
        TruckSensorsManager.Current.cautionDialogDismissed();
    
    }

    public void dismissStreetInfo()
    {
        streetInfoDialogBox.SetActive(false);
    }

    public void dismissTruckOffRouteReminder()
    {
        truckOffRouteReminderBox.SetActive(false);
    }

    static public DialogManager Current 
    {
        get
        {
            if(__Current == null)
            {
                __Current = GameObject.FindObjectOfType<DialogManager>();
            }

            return __Current;
        }
    }
}
