using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System.Linq;
using System;

public enum SensorsTypes 
{
  FrontCenterSensor,
  FrontCenterRightSensor,
  FrontCenterLeftSensor,
  RightFrontCenterSensor,
  RightFrontCenterRightSensor,
  RightFrontCenterLeftSensor,
  RightBackCenterSensor,
  RightBackCenterRightSensor,
  RightBackCenterLeftSensor,
  LeftFrontCenterSensor,
  LeftFrontCenterRightSensor,
  LeftFrontCenterLeftSensor,
  LeftBackCenterSensor,
  LeftBackCenterRightSensor,
  LeftBackCenterLeftSensor,
  BackCenterSensor,
  BackCenterRightSensor,
  BackCenterLeftSensor
}



class TruckSensorsManager : MonoBehaviour
{

  [Header("Obstacle Indicators")]
  public GameObject obstacle_indicator_up;

  public GameObject obstacle_indicator_right;

  public GameObject obstacle_indicator_down;

  public GameObject obstacle_indicator_left;

  public Material objectHighlightedMaterial;        
//Run through each gameobject's gameobjects
  private Dictionary<SensorsTypes, List<string>> truckSensors = new Dictionary<SensorsTypes, List<string>>();  
  private Dictionary<string, List<UnityEngine.Material>> objectMaterials = new Dictionary<string, List<UnityEngine.Material>>();
  static private TruckSensorsManager __Current;

  private bool isCautionDialogShown;
  private bool isStreetInfoShown;
  private CautionDialog cautionDialog;

  void OnEnable()
  {
    __Current = this;
    obstacle_indicator_up.SetActive(false);
    obstacle_indicator_right.SetActive(false);
    obstacle_indicator_down.SetActive(false);
    obstacle_indicator_left.SetActive(false);
  }

  void FixedUpdate()
  {

    foreach(KeyValuePair<SensorsTypes, List<string>> sensor in truckSensors)
    {       

        foreach(string obj in sensor.Value)
        {
          if(obj.Contains("bicycle"))
          {
            changeBicycleMaterial(obj);
            if(!isCautionDialogShown)
            {
              TruckMovement.Current.applyBraking();
              showCautionDialog(sensor.Key.ToString(), obj);
            }
          }
          
          else if(obj.Contains("car"))
          {
            changeCarMeterial(obj);
            if(!isCautionDialogShown)
            {
              TruckMovement.Current.applyBraking();
              showCautionDialog(sensor.Key.ToString(), obj);
            }
          }
          else if(obj.Contains("person"))
          {
            changePersonMeterial(obj);
            if(!isCautionDialogShown)
            {
              TruckMovement.Current.applyBraking();
              showCautionDialog(sensor.Key.ToString(), obj);
            }
          }
          
          else if(obj.Contains("s_"))
          {
            if(isStreetInfoShown)
            {
              dismissStreetInfo();
            }

            showStreetInfo(sensor.Key.ToString(), obj);
          }      
         
        }
    }

  } 

  private void showCautionDialog(string sensor, string objectName)
  {
   
    isCautionDialogShown = true;
    //Write Code to show dialog
    CautionDialogInfo cautionDialogInfo = new CautionDialogInfo(sensor, objectName);
     
    cautionDialog = new CautionDialog();
    cautionDialog.cautionDialogInfo = cautionDialogInfo;
    DialogManager.Current.StartDialog(cautionDialog);
  } 

  public void cautionDialogDismissed()
  {
    isCautionDialogShown = false;
  }

  private void showStreetInfo(string sensor, string objectName)
  {
    dismissTruckOffRouteReminder();
    isStreetInfoShown = true;

    StreetDialogInfo streetDialogInfo = new StreetDialogInfo(sensor, objectName);
    StreetInfoDialog streetInfoDialog = new StreetInfoDialog();
    streetInfoDialog.streetDialogInfo = streetDialogInfo;
    DialogManager.Current.StartDialog(streetInfoDialog);

  }

  private void dismissStreetInfo()
  {
    showTruckOffRouteReminder();
    isStreetInfoShown = false;
    DialogManager.Current.dismissStreetInfo();
  }

  private void dismissTruckOffRouteReminder()
  {
    DialogManager.Current.dismissTruckOffRouteReminder();
  }

  private void showTruckOffRouteReminder()
  {
    DialogManager.Current.startTruckOffRouteReminderDialog();
  }


  private void changeBicycleMaterial(string bicycleName)
  {
    //Create a dictionary to store each material so that 
    //every object's material can be reset to its 
    //original material when bicycle no longer
    //obstructs sensors

    GameObject bicycle = GameObject.Find(bicycleName);

    if(!objectMaterials.ContainsKey(bicycleName))
    {
      objectMaterials.Add(bicycleName, new List<UnityEngine.Material>());
    }
    foreach(Transform child in bicycle.transform)
    {
        objectMaterials[bicycleName].Add(child.gameObject.GetComponent<MeshRenderer>().material);
        
        child.gameObject.GetComponent<MeshRenderer>().material = objectHighlightedMaterial;
        // Debug.Log(child.gameObject.GetComponent<MeshRenderer>().material.GetType());

    }

  }  

  private void changeBicycleMaterialToOrginal(string bicycleName)
  {
    int i =0;
    GameObject bicycle = GameObject.Find(bicycleName);
    foreach(Transform child in bicycle.transform)
    {
      child.gameObject.GetComponent<MeshRenderer>().material = objectMaterials[bicycleName][i];
      i++;
    }
  }

  private void changeCarMeterial(string carName)
  {
    GameObject car = GameObject.Find(carName);
    if(!objectMaterials.ContainsKey(carName))
    {
      objectMaterials.Add(carName, new List<UnityEngine.Material>());
    }

    objectMaterials[carName].Add(car.GetComponent<MeshRenderer>().material);
    car.GetComponent<MeshRenderer>().material = objectHighlightedMaterial;
  }

  private void changeCarMaterialToOrginal(string carName)
  {
    GameObject car = GameObject.Find(carName);
    car.GetComponent<MeshRenderer>().material = objectMaterials[carName][0];
    
  }

   private void changePersonMeterial(string personName)
  {
    //Create a dictionary to store each material so that 
    //every object's material can be reset to its 
    //original material when person no longer
    //obstructs sensors

    GameObject person = GameObject.Find(personName);

    if(!objectMaterials.ContainsKey(personName))
    {
      objectMaterials.Add(personName, new List<UnityEngine.Material>());
    }
    foreach(Transform child in person.transform)
    {
        objectMaterials[personName].Add(child.gameObject.GetComponent<SkinnedMeshRenderer>().material);
        
        child.gameObject.GetComponent<SkinnedMeshRenderer>().material = objectHighlightedMaterial;
        
        break;

    }

  }
  private void changePersonMaterialToOrginal(string personName)
  {
    int i =0;

    GameObject person = GameObject.Find(personName);

    foreach(Transform child in person.transform)
    {
      child.gameObject.GetComponent<SkinnedMeshRenderer>().material = objectMaterials[personName][i];
      break;
    }
  }

  static public TruckSensorsManager Current {

    get
    {

        if(__Current==null)
        {
            __Current = GameObject.FindObjectOfType<TruckSensorsManager>();
        }
        return __Current;
    }

  }

  public void reportDetection(SensorsTypes sensorsType, string obstacleName)
  {
    if(!truckSensors.ContainsKey(sensorsType))
    {
        truckSensors.Add(sensorsType, new List<string>());
    }

    if(!truckSensors[sensorsType].Contains(obstacleName))
    {
        if(obstacleName.Contains("s_") && (sensorsType == SensorsTypes.FrontCenterSensor || sensorsType==SensorsTypes.FrontCenterRightSensor || sensorsType==SensorsTypes.FrontCenterLeftSensor))
        {
            truckSensors[sensorsType].Add(obstacleName);

            obstacle_indicator_up.SetActive(true);           

        }

        else if(obstacleName.Contains("person")||obstacleName.Contains("car")||obstacleName.Contains("bicycle"))
        {
            truckSensors[sensorsType].Add(obstacleName);

            if(sensorsType == SensorsTypes.FrontCenterSensor || sensorsType==SensorsTypes.FrontCenterRightSensor || sensorsType==SensorsTypes.FrontCenterLeftSensor)
            {
                obstacle_indicator_up.SetActive(true);
            }
            else if(sensorsType==SensorsTypes.RightFrontCenterSensor || sensorsType == SensorsTypes.RightFrontCenterRightSensor || sensorsType == SensorsTypes.RightFrontCenterLeftSensor || sensorsType == SensorsTypes.RightBackCenterSensor || sensorsType == SensorsTypes.RightBackCenterRightSensor || sensorsType == SensorsTypes.RightBackCenterLeftSensor)
            {
                obstacle_indicator_right.SetActive(true);
            }
            else if(sensorsType==SensorsTypes.BackCenterSensor || sensorsType==SensorsTypes.BackCenterRightSensor || sensorsType == SensorsTypes.BackCenterLeftSensor)
            {
                obstacle_indicator_down.SetActive(true);
            }
            else if(sensorsType==SensorsTypes.LeftFrontCenterSensor || sensorsType==SensorsTypes.LeftFrontCenterRightSensor || sensorsType==SensorsTypes.LeftFrontCenterLeftSensor || sensorsType == SensorsTypes.LeftBackCenterSensor || sensorsType==SensorsTypes.LeftBackCenterRightSensor || sensorsType == SensorsTypes.LeftBackCenterLeftSensor)
            {
                obstacle_indicator_left.SetActive(true);
            }

        }
     
      
      
    }
    
  }  

  public void unreportDetection(SensorsTypes sensorsType)
  {
    
    if(sensorsType == SensorsTypes.FrontCenterSensor || sensorsType==SensorsTypes.FrontCenterRightSensor || sensorsType==SensorsTypes.FrontCenterLeftSensor)
    {
      obstacle_indicator_up.SetActive(false);
    }
    else if(sensorsType==SensorsTypes.RightFrontCenterSensor || sensorsType == SensorsTypes.RightFrontCenterRightSensor || sensorsType == SensorsTypes.RightFrontCenterLeftSensor || sensorsType == SensorsTypes.RightBackCenterSensor || sensorsType == SensorsTypes.RightBackCenterRightSensor || sensorsType == SensorsTypes.RightBackCenterLeftSensor)
    {
      obstacle_indicator_right.SetActive(false);
    }
    else if(sensorsType==SensorsTypes.BackCenterSensor || sensorsType==SensorsTypes.BackCenterRightSensor || sensorsType == SensorsTypes.BackCenterLeftSensor)
    {
      obstacle_indicator_down.SetActive(false);
    }
    else if(sensorsType==SensorsTypes.LeftFrontCenterSensor || sensorsType==SensorsTypes.LeftFrontCenterRightSensor || sensorsType==SensorsTypes.LeftFrontCenterLeftSensor || sensorsType == SensorsTypes.LeftBackCenterSensor || sensorsType==SensorsTypes.LeftBackCenterRightSensor || sensorsType == SensorsTypes.LeftBackCenterLeftSensor)
    {
      obstacle_indicator_left.SetActive(false);
    }
   
    if (truckSensors.ContainsKey(sensorsType))
    {

      if(isCautionDialogShown)
      {
        DialogManager.Current.no_response();
        cautionDialogDismissed();
      }

      if(isStreetInfoShown)
      {
        dismissStreetInfo();
      }
    
      foreach(string obj in truckSensors[sensorsType])
      {
          if(obj.Contains("bicycle"))
          {
              changeBicycleMaterialToOrginal(obj);
          }
          
          else if(obj.Contains("car"))
          {
              changeCarMaterialToOrginal(obj);
          }
          
          else if(obj.Contains("person"))
          {            
              changePersonMaterialToOrginal(obj);
          }
          
      }
      truckSensors[sensorsType].Clear();
    }

  }

  public void unreportAllSensorsDetection()
  {
    var sensorsTypes = Enum.GetValues(typeof(SensorsTypes));

    foreach(int s in sensorsTypes)
    {
      unreportDetection((SensorsTypes)s);
    }
  }
  public bool isReported(SensorsTypes sensor)
  {
    if(truckSensors.ContainsKey(sensor) && truckSensors[sensor].Count>0)
    {
        return true;
    }

    return false;
  }

}