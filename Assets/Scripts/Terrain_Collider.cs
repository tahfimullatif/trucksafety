using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mapbox.Unity.Map;
using Mapbox.Utils;
public class Terrain_Collider : MonoBehaviour
{
    public AbstractMap map;
    // Start is called before the first frame update
    void Start()
    {
        GameObject terrain = this.gameObject;
    
        Vector2d[] location = new Vector2d[1];
        
        location[0] = map.CenterLatitudeLongitude;
        Vector3 y_up = new Vector3(0, 20, 0);
        terrain.transform.localPosition = map.GeoToWorldPosition(location[0], true);
        terrain.transform.localScale = new Vector3(200000, 1, 200000);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
