using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mapbox.Unity.Map;
using Mapbox.Utils;
using System;
using KdTree;
using KdTree.Math;
public class TruckRouteCubeSpawner : MonoBehaviour
{
    public AbstractMap map;
    public TextAsset geo;
    public TextAsset Streets;
    public TextAsset routes;
    string[] streets;
    double[,] geocoordinates;
    string[] route_types;
    bool dataRead;
    bool isObjectsCreated;
    bool isMapCenterSet;
    KdTree<float, string> tree;

    static private TruckRouteCubeSpawner __Current;
    // Start is called before the first frame update
    void Start()
    {

        if(!dataRead)
        {
            tree= new KdTree<float, string>(2, new FloatMath());
            readGeocoordinates();
            readStreets();
            readRouteTypes();
            dataRead = true;
        }
      
    }

    void readGeocoordinates()
    {
        var Geocoordinates = geo.text.Split("\n");
        geocoordinates = new double[Geocoordinates.Length, 2];
        int index = 0;
        foreach(var geocoordinate in Geocoordinates)
        {   
            string[] Geocoordinate = geocoordinate.Split(',');
            if(Geocoordinate.Length==2)
            {
                geocoordinates[index, 0] = Convert.ToDouble(Geocoordinate[0]);
                geocoordinates[index, 1] = Convert.ToDouble(Geocoordinate[1]);
                // tree.Add(new float[] { ()geocoordinates[index, 1], geocoordinates[index, 0] }, "");
            } 

            index++;
            
        }
    }

    void readStreets()
    {
        var _streets = Streets.text.Split("\n");
        streets = new string[_streets.Length];
        int index = 0;
        foreach(var street in _streets)
        {
            streets[index] = "s_"+street;
            index++;
        }

    }

    void readRouteTypes()
    {
       var _route_types = routes.text.Split("\n");
       route_types = new string[_route_types.Length];
       int index = 0;
       foreach(var route_type in _route_types)
       {
            route_types[index] = "r_"+route_type;
            index++;
       }     
    }

    // Update is called once per frame
    void Update()
    {
        if(!isMapCenterSet)
        {
            map.UseCustomScale(2000);
            Vector2d bronx_center_map = new Vector2d(40.854749995233384, -73.8769837319304);
            map.SetCenterLatitudeLongitude(bronx_center_map);
            map.UpdateMap();
            isMapCenterSet = true;
        }
        
        if(!isObjectsCreated)
        {

            isObjectsCreated = true;
            TruckMovementOnTruckRoute.Current.moveTruckToPosition();
            Debug.Log("Cubes are being created");
            for(int i=0; i<streets.Length; i++)
            {
                GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
                cube.name = streets[i]+" type "+route_types[i];
                Vector2d[] location = new Vector2d[1];
                location[0] = new Vector2d(geocoordinates[i,1], geocoordinates[i, 0]);
                Vector3 y_up = new Vector3(0, 20, 0);
                cube.transform.localPosition = map.GeoToWorldPosition(location[0], true);
               
                cube.transform.localScale = new Vector3(40, 2, 20);

                
                // tree.Add(new float[] { cube.transform.localPosition[0], cube.transform.localPosition[2] }, " ");



            }
        }
        
    }

    public float[] getNearestCube(float[] currTruckLocation)
    {
        var nearestNeighbour = tree.GetNearestNeighbours(
					new float[] { currTruckLocation[0], currTruckLocation[1] },
					1);
        foreach(var neighbour in nearestNeighbour)
        {
            Debug.Log("||||||||||||||||||||||");
            Debug.Log(neighbour.Point[0]);
            Debug.Log(neighbour.Point[1]);
            Debug.Log("||||||||||||||||||||||");    
        }            

        return new float[]{nearestNeighbour[0].Point[0], nearestNeighbour[0].Point[1]};
    }

    static public TruckRouteCubeSpawner Current {

		get
		{

			if(__Current==null)
			{
				__Current = GameObject.FindObjectOfType<TruckRouteCubeSpawner>();
			}
			return __Current;
		}

  	}


}
