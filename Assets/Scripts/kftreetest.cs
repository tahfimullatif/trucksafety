using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using KdTree;
using KdTree.Math;
public class kftreetest : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
    
    	KdTree<float, string> tree= new KdTree<float, string>(2, new FloatMath());
    	var toowoomba = new City()
			{
				Address = "Toowoomba, QLD, Australia",
				Lat = -27.5829487f,
				Long = 151.8643252f,
				DistanceFromToowoomba = 0
			};
    
   
	City[] cities = new City[]
			{
				toowoomba,
				new City()
				{
					Address = "Brisbane, QLD, Australia",
					Lat = -27.4710107f,
					Long = 153.0234489f,
					DistanceFromToowoomba = 1.16451615177537f
				},
				new City()
				{
					Address = "Goldcoast, QLD, Australia",
					Lat = -28.0172605f,
					Long = 153.4256987f,
					DistanceFromToowoomba = 1.6206523211724f
				},
				new City()
				{
					Address = "Sunshine, QLD, Australia",
					Lat = -27.3748288f,
					Long = 153.0554193f,
					DistanceFromToowoomba = 1.20913979664506f
				},
				new City()
				{
					Address = "Melbourne, VIC, Australia",
					Lat = -37.814107f,
					Long = 144.96328f,
					DistanceFromToowoomba = 12.3410301438779f
				},
				new City()
				{
					Address = "Sydney, NSW, Australia",
					Lat = -33.8674869f,
					Long = 151.2069902f,
					DistanceFromToowoomba = 6.31882185929341f
				},
				new City()
				{
					Address = "Perth, WA, Australia",
					Lat = -31.9530044f,
					Long = 115.8574693f,
					DistanceFromToowoomba = 36.2710774395312f
				},
				new City()
				{
					Address = "Darwin, NT, Australia",
					Lat = -12.4628198f,
					Long = 130.8417694f,
					DistanceFromToowoomba = 25.895292049265f
				}
				/*,
				new City()
				{
					Address = "London, England",
					Lat = 51.5112139f,
					Long = -0.1198244f,
					DistanceFromToowoomba = 171.33320836029f
					
				}*/
			};
			
			foreach (var city in cities)
			{
				tree.Add(new float[] { city.Long, -city.Lat }, city.Address);
			}	

			var actualNeighbours = tree.GetNearestNeighbours(
					new float[] { toowoomba.Long, -toowoomba.Lat },
					2);
					
			

			for(int i=0; i<actualNeighbours.Length; i++)
			{
				Debug.Log("-----------------------------------");
				Debug.Log(actualNeighbours[i].Point[0]);
				Debug.Log(actualNeighbours[i].Point[1]);
				Debug.Log(actualNeighbours[i].Value);
				Debug.Log("----------------------------------");		
				
			}

				
			
			
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
