using UnityEngine;
using System;
public class SpawnObjects : MonoBehaviour
{
 public GameObject person;
 public GameObject bicycle;
 public int numObjects;
 string[] objectType = new string[] {"Adult", "Kid", "Bicycle"};

 void Start()
 {
   person.transform.localScale = new Vector3(20,20,20);
   
   for(int i=0; i<numObjects; i++)
   {
     var rand = new System.Random();
     int randomIndex = rand.Next(3);

     string obj = objectType[randomIndex];

     float randomX = UnityEngine.Random.Range(-1000, 1000);
     float randomZ = UnityEngine.Random.Range(-1000, 1000);
     
     if(obj.Contains("Adult"))
     {

        GameObject p = Instantiate(person, new Vector3(randomX, 50f, randomZ), Quaternion.identity) as GameObject; 
        p.name = String.Concat("person_", i);
        p.transform.localScale = new Vector3(6, 6, 6);
        BoxCollider bc = (BoxCollider)p.AddComponent(typeof(BoxCollider));
        bc.size = new Vector3(1, 5, 1); 
     }
     else if(obj.Contains("Kid"))
     {
        GameObject p = Instantiate(person, new Vector3(randomX, 50f, randomZ), Quaternion.identity) as GameObject; 
        p.name = String.Concat("person_", i);
        p.transform.localScale = new Vector3(4, 4, 4);
        BoxCollider bc = (BoxCollider)p.AddComponent(typeof(BoxCollider));
        bc.size = new Vector3(1, 5, 1); 
     
     }

     else if(obj.Contains("Bicycle"))
     {
        GameObject p = Instantiate(bicycle, new Vector3(randomX, 50f, randomZ), Quaternion.identity) as GameObject; 
        p.name = String.Concat("bicycle_", i);
        p.transform.localScale = new Vector3(5, 5, 5);
        BoxCollider bc = (BoxCollider)p.AddComponent(typeof(BoxCollider));
        bc.size = new Vector3(1, 5, 1);   
       
     }
     
   }

 }

}
